[![built with gulp.js](https://img.shields.io/badge/built%20with-gulp.js-green.svg)](http://gulpjs.com/)
[![build status](https://travis-ci.org/lightspeeddevelopment/minimal-lsx.svg?branch=master)](https://travis-ci.org/lightspeeddevelopment/minimal-lsx)

# MINIMAL LSX

MINIMAL LSX Child Theme.

## Changelog

### 1.1.5
- Adding option in customizer to turn off the home demo slider

### 1.1.3 
- Wordpress Ready

### 1.0.0 - 06/07/17
- First Version

## Setup

### 1: Install NPM
https://nodejs.org/en/

### 2: Install Gulp
- `npm install`

This will run the package.json file and download the list of modules to a "node_modules" folder in the plugin.

### 3: Gulp Commands
- `gulp watch`
- `gulp compile-css`
- `gulp compile-js`
- `gulp wordpress-lang`
