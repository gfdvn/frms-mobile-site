<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'hoh');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<I^NR{auW-EqF~~al!6Pd6;+Yef^~fK&8NM=M(kxpt`hk6n;JB*0^K*3U@#a 8z?');
define('SECURE_AUTH_KEY',  '[Tah9dPk| f]5tQW,:Yi1YmT]*}/5([U<5HxV?jDa E.dpp)Z>vbM3[b3<t-[/7[');
define('LOGGED_IN_KEY',    '3I%LkKD-Clhrrw9rT=U#Dr|+UASRH#lZ0Lqgp^n^b8[cbePPzljH5z{p-?Z![{qz');
define('NONCE_KEY',        '1I{Yd=ArFV_~vN)>h-,fUUCR85}E/>:jy7.6UqHj-,EwlnPbt}w6.;[tq5c3)_#J');
define('AUTH_SALT',        'bk;@3%hY^Ye_M75BcS!ax;CygU&8z 4CMt[bT?_EqP{IDai]8:_d`!?l!5b!mj5W');
define('SECURE_AUTH_SALT', 'dz1KekC)9Zc:Ne&`P41P[Yy1)G,}{DfN!R(~Rdy:Qbwq&,I/AOk$27mqM]rmCex>');
define('LOGGED_IN_SALT',   'm|m4^*jHd?W&Lyf+/k?- d#%$AY(7r]*E|lv8S-]^ZRr*_TEQh1,GJUi/iA-cxwm');
define('NONCE_SALT',       'ec93WmXmsQN~+&iJ_n:sgr|;a=&TWH`wXh_X[|}Qt &)~iFQUyF:5Cwcc)q$O1eR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'FS_METHOD', 'direct' );
