<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package coupontray
 */
	  get_header(); ?>
	  
	  
        <main id="main" class="site-main">
        <div class="space"></div>
          <div class="row">
            <div class="col s12 m8 l8">
			<?php if ( have_posts() ) : 
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			
				get_template_part( 'template-parts/content', get_post_format() );	
				
				endwhile;
				
				
			else :
			
				get_template_part( 'template-parts/content', 'none' ); ?>
				
			
			  <?php endif; ?>
            </div><!-- /.col .s12 .m6 .l4 -->			
            <div class="col s12 m4 l4">
              <div class="sidebar">
				<?php get_sidebar(); ?>
			  </div>
            </div>
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
		
      </div><!-- /#primary /.content-area -->


<?php
get_footer();
