<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package coupontray
 */
?>

<footer class="ct-sidebar-footer">
        <div class="row ctbottom">
          <div class="col s12 m6 l3">
			<aside id="secondary1" class="widget-area first s-footer" role="complementary">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</aside><!-- /.first-footer -->
          </div><!-- /.col .s12 .m6 .l3 -->
          <div class="col s12 m6 l3">
			<aside id="secondary2" class="widget-area second s-footer" role="complementary">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</aside><!-- /.second s-footer -->
          </div><!-- /.col .s12 .m6 .l3 -->
          <div class="col s12 m6 l3">
            <aside id="secondary3" class="widget-area third s-footer" role="complementary">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</aside><!-- /.third s-footer -->
          </div><!-- /.col .s12 .m6 .l3 -->
          <div class="col s12 m6 l3">
            <aside id="secondary4" class="widget-area fourth s-footer" role="complementary">
				<?php dynamic_sidebar( 'sidebar-5' ); ?>
			</aside><!-- /.fourth s-footer -->
          </div><!-- /.col .s12 .m6 .l3 -->

        </div><!-- /.row -->
      </footer><!-- /.ct-sidebar-footer -->