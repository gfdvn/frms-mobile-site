<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package coupontray
 */

get_sidebar('footer')?>

        <div class="footer-copyright">
            <p><a href="<?php echo esc_url( __( 'http://smallenvelop.com/', 'coupontray' ) ); ?>" title="<?php esc_attr_e( 'SmallEnvelop', 'coupontray' ); ?>"><?php printf( __( 'Coupontray %s', 'coupontray' ), 'Theme ' ); ?></a><a href="<?php echo esc_url( __( 'http://wordpress.org/', 'coupontray' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'coupontray' ); ?>"><?php printf( __( 'proudly powered by %s', 'coupontray' ), 'WordPress' ); ?></a></p>
        </div>
      </div><!-- /.global-inner -->
    </div><!-- /.container -->
  </div><!-- /.ct-wrapper .page -->
	<?php wp_footer(); ?>
  </body>
</html>