<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package coupontray
 */

get_header(); ?>
	
        <main id="main" class="site-main">
        <div class="space"></div>
          <div class="row">
            <div class="col s12 m8 l8">

					<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'single' );


							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
					?>
			  
            </div><!-- /.col .s12 .m6 .l4 -->
            <div class="col s12 m4 l4">
              <div class="sidebar">
				<?php get_sidebar(); ?>
			  </div>
            </div>
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
      </div><!-- /#primary /.content-area -->
	  
<?php get_footer();






