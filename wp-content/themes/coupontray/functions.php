<?php
/**
 * coupontray functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package coupontray
 */
/* ------------------------------------------------------------------------- *
 *  Custom functions
/* ------------------------------------------------------------------------- */
	
	// Use a child theme instead of placing custom functions here
	// http://codex.wordpress.org/Child_Themes
/* ------------------------------------------------------------------------- *
 *  Base functionality
/* ------------------------------------------------------------------------- */
if ( ! function_exists( 'coupontray_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function coupontray_setup() {
	// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' ); 
	// Enable title tag
		add_theme_support( 'title-tag' );
	// Custom Logo
		add_theme_support( 'custom-logo', array(
			'height'      => 100,
			'width'       => 400,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );
	// Enable featured image
		add_theme_support( 'post-thumbnails' );
	// Enable thumbnail image size
		add_image_size( 'coupontray_featuredhome', 793, 320, true); //featured big
		/*
	 * This theme supports custom background color and image,
	 * and here we also set up the default background color.
	 */
		add_theme_support( 'custom-background', array(
			'default-color' => 'e6e6e6',
			'default-image' => '%1$s/img/food-icon.png',
		) );

	// Custom menu areas
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'coupontray' ),
			'mobile' => esc_html__( 'Mobile', 'coupontray' ),
		) );
	//Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
	//Enable support for Post Formats.
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

	// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'coupontray_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
}
endif;
add_action( 'after_setup_theme', 'coupontray_setup' );


/* Set the content width in pixels, based on the theme's design and stylesheet.
/* --------------------------------------------------------------------------*/
function coupontray_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'coupontray_content_width', 640 );
}
add_action( 'after_setup_theme', 'coupontray_content_width', 0 );

/* Register widget area.
/* --------------------------------------------------------------------------*/
function coupontray_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'coupontray' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'First Footer Sidebar', 'coupontray' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Second Footer Sidebar', 'coupontray' ),
		'id'            => 'sidebar-3',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Third Footer Sidebar', 'coupontray' ),
		'id'            => 'sidebar-4',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Fourth Footer Sidebar', 'coupontray' ),
		'id'            => 'sidebar-5',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	
}
add_action( 'widgets_init', 'coupontray_widgets_init' );

/* Enqueue scripts and styles.
/* --------------------------------------------------------------------------*/
function coupontray_scripts() {
	wp_enqueue_style( 'materialize', get_template_directory_uri().'/css/materialize.css' );
	wp_enqueue_style( 'material-icons', get_template_directory_uri().'/font/material-icons.css' ); 
	wp_enqueue_style( 'monoton', 'https://fonts.googleapis.com/css?family=Monoton' ); 
	wp_enqueue_style( 'coupontray-style', get_stylesheet_uri() ); 
	wp_enqueue_script( 'coupontray-custom-scripts', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ),'', true );
	wp_enqueue_script( 'materialize-scripts', get_template_directory_uri() . '/js/materialize.js', array( 'jquery' ),'', true );
	wp_enqueue_script( 'hoverIntent', array( 'jquery' ),'', true );
	wp_enqueue_script( 'superfish-scripts', get_template_directory_uri() . '/js/superfish.js', array( 'jquery' ),'', true );
	wp_enqueue_script( 'mmenu-scripts', get_template_directory_uri() . '/js/jquery.mmenu.min.all.js', array( 'jquery' ),'', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'coupontray_scripts' );

/* Implement the Custom Header feature.
/* --------------------------------------------------------------------------*/
require get_template_directory() . '/inc/custom-header.php';

/* Custom template tags for this theme.
/* --------------------------------------------------------------------------*/
require get_template_directory() . '/inc/template-tags.php';

/* Custom functions that act independently of the theme templates.
/* --------------------------------------------------------------------------*/
require get_template_directory() . '/inc/extras.php';

/* Customizer additions.
/* --------------------------------------------------------------------------*/
require get_template_directory() . '/inc/customizer.php';

/* Load Jetpack compatibility file.
/* --------------------------------------------------------------------------*/
require get_template_directory() . '/inc/jetpack.php';
/*-----------------------------------------------------------------
 * Editors Stylsheet
-----------------------------------------------------------------*/
function coupontray_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'coupontray_add_editor_styles' );

/*-----------------------------------------------------------------
 * Default Thumbnails
-----------------------------------------------------------------*/
function coupontray_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }
    
    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }
        
    // use no preview fallback
    if ( file_exists( get_template_directory().'/images/nothumb-'.$size.'.png' ) )
        return get_template_directory_uri().'/images/nothumb-'.$size.'.png';
    else
        return '';
}
/*-----------------------------------------------------------------
 * Breadcrumb
-----------------------------------------------------------------*/
if (!function_exists('coupontray_breadcrumb')) {
    function coupontray_breadcrumb() {
        echo '<div typeof="v:Breadcrumb" class="root"><a rel="v:url" property="v:title" href="';
        echo home_url();
        echo '" rel="nofollow">'.sprintf( __( "Home", 'coupontray' ));
        echo '</a></div><div><i class="material-icons">keyboard_arrow_right</i></div>';
        if (is_single()) {
            $categories = get_the_category();
            if ( $categories ) {
                $level = 0;
                $hierarchy_arr = array();
                foreach ( $categories as $cat ) {
                    $anc = get_ancestors( $cat->term_id, 'category' );
                    $count_anc = count( $anc );
                    if (  0 < $count_anc && $level < $count_anc ) {
                        $level = $count_anc;
                        $hierarchy_arr = array_reverse( $anc );
                        array_push( $hierarchy_arr, $cat->term_id );
                    }
                }
                if ( empty( $hierarchy_arr ) ) {
                    $category = $categories[0];
                    echo '<div typeof="v:Breadcrumb"><a href="'. esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="material-icons">keyboard_arrow_right</i></div>';
                } else {
                    foreach ( $hierarchy_arr as $cat_id ) {
                        $category = get_term_by( 'id', $cat_id, 'category' );
                        echo '<div typeof="v:Breadcrumb"><a href="'. esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="material-icons">keyboard_arrow_right</i></div>';
                    }
                }
            }
            echo "<div><span>";
            the_title();
            echo "</span></div>";
        } elseif (is_page()) {
            global $post;
            if ( $post->post_parent ) {
                $parent_id  = $post->post_parent;
                $breadcrumbs = array();
                while ( $parent_id ) {
                    $page = get_page( $parent_id );
                    $breadcrumbs[] = '<div typeof="v:Breadcrumb"><a href="'.esc_url( get_permalink( $page->ID ) ).'" rel="v:url" property="v:title">'.esc_html( get_the_title($page->ID) ). '</a></div><div><i class="material-icons">keyboard_arrow_right</i></div>';
                    $parent_id  = $page->post_parent;
                }
                $breadcrumbs = array_reverse( $breadcrumbs );
                foreach ( $breadcrumbs as $crumb ) { echo $crumb; }
            }
            echo "<div><span>";
            the_title();
            echo "</span></div>";
        } elseif (is_category()) {
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $this_cat_id = $cat_obj->term_id;
            $hierarchy_arr = get_ancestors( $this_cat_id, 'category' );
            if ( $hierarchy_arr ) {
                $hierarchy_arr = array_reverse( $hierarchy_arr );
                foreach ( $hierarchy_arr as $cat_id ) {
                    $category = get_term_by( 'id', $cat_id, 'category' );
                    echo '<div typeof="v:Breadcrumb"><a href="'.esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="material-icons">keyboard_arrow_right</i></div>';
                }
            }
            echo "<div><span>";
            single_cat_title();
            echo "</span></div>";
        } elseif (is_author()) {
            echo "<div><span>";
            if(get_query_var('author_name')) :
                $curauth = get_user_by('slug', get_query_var('author_name'));
            else :
                $curauth = get_userdata(get_query_var('author'));
            endif;
            echo esc_html( $curauth->nickname );
            echo "</span></div>";
        } elseif (is_search()) {
            echo "<div><span>";
            the_search_query();
            echo "</span></div>";
        } elseif (is_tag()) {
            echo "<div><span>";
            single_tag_title();
            echo "</span></div>";
        }
    }
}
/*-------------------------------------------------------------*/
/*  Nav fallback
/*-------------------------------------------------------------*/
function coupontray_primary_nav_fallback() {
    ?>
    <ul class="sf-menu ct-menu-desk" id="example">
        <?php
        wp_list_pages('title_li=&show_home=1&sort_column=menu_order');
        ?>
    </ul>

    <?php
}
function coupontray_mobile_nav_fallback() {
    ?>
    <ul>
        <?php
        wp_list_pages('title_li=&show_home=1&sort_column=menu_order');
        ?>
    </ul>

    <?php
}
/*-------------------------------------------------------------*/
/*  Add current class to current menu item
/*-------------------------------------------------------------*/
add_filter('nav_menu_css_class' , 'coupontray_special_nav_class' , 10 , 2);
function coupontray_special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'current ';
     }
     return $classes;
}

/*-------------------------------------------------------------*/
/*  Comment Form
/*-------------------------------------------------------------*/
function coupontray_display_custom_comments($comment, $args, $depth) {

    $isByAuthor = false;

    if($comment->comment_author_email == get_the_author_meta('email')) {
        $isByAuthor = true;
    }

   $GLOBALS['comment'] = $comment; ?>

   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>" <?php if($isByAuthor){ echo 'class="author"';}?>>
      <div class="comment-author vcard">
         <?php echo get_avatar( $comment->comment_author_email, 100 ); ?>

         <?php printf(__('<cite class="comment-name">%s</cite>', 'coupontray'), get_comment_author()) ?><?php edit_comment_link(__('(Edit)', 'coupontray'),'  ','') ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.', 'coupontray') ?></em>
         <br />
      <?php endif; ?>
		<div class="comment-date"><span><?php comment_date('F j, Y'); ?> <?php _e('at', 'coupontray') ?> <?php comment_time(  );?><span></div>
		<div class="comment-text"><?php comment_text() ?></div>
      

      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </div>
    </li>
<?php
}
/*-------------------------------------------------------------*/
/*  Custom Logo
/*-------------------------------------------------------------*/
function coupontray_custom_logo() {
    // Try to retrieve the Custom Logo
    $output = '';
    if (function_exists('the_custom_logo'))
        $output = the_custom_logo();

    // Nothing in the output: Custom Logo is not supported, or there is no selected logo
    // In both cases we display the site's name
    if (empty($output))
		$description = get_bloginfo( 'description', 'display' );
        $output = '<h1><a href="' . esc_url(home_url('/')) . '">' . get_bloginfo('name') . '</a></h1>';
		$description = get_bloginfo( 'description', 'display' );
		if ( $description || is_customize_preview() ) : 
		$output2= '<p class="site-description">' . $description . '</p>';
		endif;

    echo $output . '' . $output2;
}
