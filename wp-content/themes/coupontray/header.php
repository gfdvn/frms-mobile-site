<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package coupontray
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta itemprop="name" content="<?php bloginfo( 'name' ); ?>" />
	<meta itemprop="url" content="<?php echo site_url(); ?>" />
	<?php if ( is_singular() ) { ?>
	<meta itemprop="creator accountablePerson" content="<?php $user_info = get_userdata($post->post_author); echo $user_info->first_name.' '.$user_info->last_name; ?>" />
	<?php }
	wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

  <div class="ct-wrapper page" role='main'>
    <div class="container">
      <div class="global-inner ctbg">
        <section class="ct-top-main-logo">
            <div class="row ctbottom">
              <div class="col s12">
                <div class="ct-logo center-align">
				<?php coupontray_custom_logo(); ?>
                </div>
              </div>
            </div>
        </section>
        <header>
          <div class="row ctbottom">
            <div class="col s12">
              <div class="ct-menu">
			  <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'example', 'menu_class' => 'sf-menu ct-menu-desk', 'fallback_cb' => 'coupontray_primary_nav_fallback' ) ); ?>
                
                <div class="header mmenu-button center">
                  <a class="btn-floating btn-medium waves-effect waves-light blue-grey lighten-1" href="#menu"><i class="material-icons">dashboard</i></a>
                  <div class="ct-mobile-menu" id="menu">
					<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'fallback_cb' => 'coupontray_mobile_nav_fallback' ) ); ?>
                     
                   </div><!-- /.ct-mobile-menu /#menu -->
                </div><!-- /.header .mmenu-button -->
              </div><!-- /.ctbg .ct-menu -->
            </div><!-- /.col .s12 -->
          </div><!-- /.row -->
        </header>
		
	<div id="primary" class="content-area home">
        <?php if ( get_header_image() ) : ?>
			<div class="page-banner">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php header_image(); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
			</div>
		<?php endif; ?>
		
        <div class="card-title blue-grey">
          <div class="row ctbottom">
            <div class="col s12">
              <header class="page-title ct-breadcrumb">
                <?php coupontray_breadcrumb() ?>
              </header>
            </div>
          </div>
        </div>