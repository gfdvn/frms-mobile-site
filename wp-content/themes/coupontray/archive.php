<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package coupontray
 */

get_header(); ?>

	
        <main id="main" class="site-main">
        <div class="space"></div>
          <div class="row">
            <div class="col s12 m8 l8">
			
			<?php
			if ( have_posts() ) : ?>

				<header class="page-header archive headings">
					<?php
						the_archive_title( '<h4 class="page-title">', '</h4>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
			  
            </div><!-- /.col .s12 .m6 .l4 -->
            <div class="col s12 m4 l4">
              <div class="sidebar">
				<?php get_sidebar(); ?>
			  </div>
            </div>
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
      </div><!-- /#primary /.content-area -->

<?php
get_footer();







		
