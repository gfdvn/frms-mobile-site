<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package coupontray
 */

?>
  <article id="post-<?php the_ID(); ?>" <?php post_class("post-home"); ?>>
	  <header class="entry-header center">
		<div class="entry-cat home">
		  <em><?php echo get_the_category_list(', '); ?></em>
		</div>
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			} ?>
		<div class="entry-author">
		  <span class="item-author"><?php esc_html_e('written by ', 'coupontray'); ?><?php the_author_posts_link(); ?></span>
		</div><!-- /.entry-author -->
	  </header>
	<?php if (is_single()) {} else { ?>
		<div class="entry-thumb">
			<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail('coupontray_featuredhome');
				} else if (coupontray_get_thumbnail_url()=='')  { } else { ?>
					<img src="<?php echo coupontray_get_thumbnail_url('coupontray_featuredhome'); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
				<?php } ?>
		</div>
	<?php } ?>
	<div class="entry-content-inner">
	  
			<?php if ( is_home() ) { ?>
				<div class="entry-content excerpt">
				<?php the_excerpt();
				} else { ?>
					<div class="entry-content">
					<?php the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'coupontray' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'coupontray' ),
						'after'  => '</div>',
					) );
					}
			?>
		</div>
		<?php if (is_single()) {} else { ?>
		  <div class="keep-reading center">
			<p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php esc_html_e('CONTINUE READING', 'coupontray'); ?></a></p>
		  </div>
		<?php } ?>
	  <div class="entry-meta home">
		<ul class="meta-content">
		  <li class="entry-date"><i class="material-icons">av_timer</i><span><?php echo get_the_date(); ?></span></li>
		  <li class="entry-comment"><i class="material-icons">comment</i><span><?php comments_number( 'no responses', 'one response', '% responses' ); ?></span></li>
		</ul>
	  </div>
	</div><!-- /.entry-content-inner -->
  </article>