# FRMS Mobile Site

Deployment guide:

- Pull this repo to local machine
- On target machine:
  > mysql -u <user> [-p <pass>]
  > CREATE DATABASE wordpress;
  > EXIT
  > mysql -u <user> [-p <pass>] wordpress < MYSQL_BACKUP.sql
- Copy files & folders in _frms-mobile-site_ folder to root of webserver
