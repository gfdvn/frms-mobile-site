<?php
/**
 * Template Name: Fullwidth
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package coupontray
 */


get_header(); ?>
	
        <main id="main" class="site-main full-page">
          <div class="row">
            <div class="col s12 m12 l12">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			  
            </div><!-- /.col .s12 .m6 .l4 -->
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
      </div><!-- /#primary /.content-area -->
	  
<?php get_footer();