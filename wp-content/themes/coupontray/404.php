<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package coupontray
 */

get_header(); ?>

	<main id="main" class="site-main">
        <div class="space"></div>
          <div class="row">
            <div class="col s12 m8 l8">

				<section class="error-404 not-found">
				<header class="page-header 404 headings">
					<h4 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'coupontray' ); ?></h4>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'coupontray' ); ?></p>

					<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( coupontray_categorized_blog() ) :
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'coupontray' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->

					<?php
						endif;

						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'coupontray' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			  
            </div><!-- /.col .s12 .m6 .l4 -->
            <div class="col s12 m4 l4">
              <div class="sidebar">
				<?php get_sidebar(); ?>
			  </div>
            </div>
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
      </div><!-- /#primary /.content-area -->

<?php
get_footer();





