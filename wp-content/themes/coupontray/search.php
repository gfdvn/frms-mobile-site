<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package coupontray
 */

get_header(); ?>

	<main id="main" class="site-main">
        <div class="space"></div>
          <div class="row">
            <div class="col s12 m8 l8">
			
			<?php
				if ( have_posts() ) : ?>

					<header class="page-header search headings">
						<h4 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'coupontray' ), '<span>' . get_search_query() . '</span>' ); ?></h4>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
			  
            </div><!-- /.col .s12 .m6 .l4 -->
            <div class="col s12 m4 l4">
              <div class="sidebar">
				<?php get_sidebar(); ?>
			  </div>
            </div>
          </div><!-- /.row -->
        </main><!-- /#main /.site-main -->
      </div><!-- /#primary /.content-area -->

<?php
get_footer();



