=== Coupontray ===
=== A Blog WordPress Theme Based on Material design

Contributor: SmallEnvelop
Requires at least: 3.8
Tested up to: 4.6
Stable tag: 1.0.3
License: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Coupontray is a responsive 100% high resolution theme for coupon and deals blogs theme. Unique toggle sidebars give a great browsing and reading experience on both tablet and mobile. The feature list is long: Unlimited topbar, header, footer and accent colors, unlimited widget areas, 0-2 sidebars to the left or right that can be uniquely specified for each page or post, 0-4 footer widget columns, good SEO, 3 flexible custom widgets, localisation support, social links, logo upload and many more useful admin panel features.
Make it yours with a custom menu, header image, logo and background. Use customizer to customize your home page.
Your latest post on —> Settings —> Reading. 

License and resources links for images:
---------------------------------------
All images licensed under GPL.


== Features ==
* Theme Customizer
* Minimalist-content-focused Material design
* Responsive layout
* Custom header
* Custom background
* Custom color via theme customizer
* Custom menu
* 2 widget areas
* Translation-ready

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Coupontray in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Support and Contribute ==
- Any problems? Go to [theme support mail](http://smallenvelop.com/contact/)

== Components ==
- Genericons, GPL2 - http://genericons.com

- _s -
=======
* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)

- * jQuery mmenu v5.5.3 -
==========================
 * mmenu.frebsite.nl
 *	
 * Copyright (c) Fred Heusschen
 * www.frebsite.nl
 *
 * Licensed under the MIT license:
 * http://en.wikipedia.org/wiki/MIT_License
 ==========================================
 
- * Materialize v0.97.5 (http://materializecss.com) -
======================================================
 * Copyright 2014-2015 Materialize
 * MIT License (https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE)
=====================================================================================

- * jQuery Superfish Menu Plugin -
================================
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
=======================================================

Image credits
=================
clock-1274699_1920.jpg
https://pixabay.com/en/clock-wall-clock-watch-time-old-1274699/

food-icon.png
By Smallenvelop